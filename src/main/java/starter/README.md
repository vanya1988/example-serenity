### Application code
 
These packages generally contain application code. 
If you are writing a reusable test library, you can also place reusable test components such as Page Objects or Tasks here.

### What refactored

- Core layer and configuration layer are moved to main/java package to separate them from test layer.
- Created abstractions for Cars API to make code reusable, easy to read and maintain.
- Added GET response model for easy validation.
- AbstractResponseWrapper created (but not used) as example how I would implement response wrapper without SerenityRest.
- Using aeonbits library to speed up a bit process of implementation for configuration layer.
- Added Hooks class where some @Before and @After scenarios stored.
- Added glue option to Test Runner.
- Renamed post_product feature file to get_product to be precise what we test.
- Refactored step definition keywords to be more precise what specific step is doing.
- Added missing dependencies to pom and gradle files and resolve conflict versions.


### Run with maven
```bash
mvn clean compile verify -e -Denv=common
```

### Run with gradle
```bash
./gradlew :test -Denv=common
```
